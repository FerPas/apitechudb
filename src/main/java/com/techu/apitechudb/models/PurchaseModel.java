package com.techu.apitechudb.models;

import java.util.HashMap;
import java.util.Map;

public class PurchaseModel {

    private String id;
    private String UserId;
    private float amount;
    private Map<String, Integer> purchaseItems;

    public PurchaseModel() {

    }

    public PurchaseModel(String id, String userId, float amount, Map<String, Integer> purchaseItems) {
        this.id = id;
        UserId = userId;
        this.amount = amount;
        this.purchaseItems = purchaseItems;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public Map<String, Integer> getPurchaseItems() {
        return purchaseItems;
    }

    public void setPurchaseItems(Map<String, Integer> purchaseItems) {
        this.purchaseItems = purchaseItems;
    }
}

package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.ProductRepository;
import com.techu.apitechudb.repositories.UserRepository;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service

public class UserService {
    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAllUser() {
        System.out.println("findAll en UserService");

        return this.userRepository.findAllUser();
    }

    public Optional<UserModel> findById(String id) {
        System.out.println("findById en ProductService");

        return this.userRepository.findById(id);
    }

    public Optional<UserModel> getById(String id) {
        System.out.println("getById en ProductService");

        return this.userRepository.findById(id);
    }
    public UserModel add(UserModel user) {
        System.out.println("add en UserService");

        return this.userRepository.save(user);
    }

    public UserModel update(UserModel user) {
        System.out.println("update en UserService");

        return this.userRepository.update(user);
    }

    public boolean delete(String id) {
        System.out.println("delete en userService");

        boolean result = false;

        Optional<UserModel> userToDelete = this.findById(id);

        if (userToDelete.isPresent() == true) {
            result = true;
            this.userRepository.delete(userToDelete.get());
        }
        return result;

    }
}

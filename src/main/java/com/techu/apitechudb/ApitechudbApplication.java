package com.techu.apitechudb;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.UserModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.HashMap;

@SpringBootApplication
public class ApitechudbApplication {

	public static ArrayList<ProductModel> productModels;
	public static ArrayList<PurchaseModel> purchaseModels;
	public static ArrayList<UserModel> userModels;


	public static void main(String[] args) {
		SpringApplication.run(ApitechudbApplication.class, args);

		ApitechudbApplication.productModels = ApitechudbApplication.getTestData();
		ApitechudbApplication.userModels = ApitechudbApplication.getTestDataUser();
		ApitechudbApplication.purchaseModels = ApitechudbApplication.getTestDataPurchase();

	}

	private static ArrayList<ProductModel> getTestData(){

		ArrayList<ProductModel> productModels = new ArrayList<>();

		productModels.add(
				new ProductModel(
						"1",
						"Producto 1",
						10
				)
		);

		productModels.add(
				new ProductModel(
						"2",
						"Producto 2",
						20
				)
		);

		productModels.add(
				new ProductModel(
						"3",
						"Producto 3",
						30
				)
		);

		return productModels;

	}


	private static ArrayList<UserModel> getTestDataUser(){

		ArrayList<UserModel> userModels = new ArrayList<>();

		userModels.add(
				new UserModel(
						"1",
						"Pepe",
						10
				)
		);

		userModels.add(
				new UserModel(
						"2",
						"Luis",
						20
				)
		);

		userModels.add(
				new UserModel(
						"3",
						"Juan",
						30
				)
		);

		return userModels;

	}


	private static ArrayList<PurchaseModel> getTestDataPurchase() {

	return new ArrayList();
	}

}

package com.techu.apitechudb.controllers;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.ProductService;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v3")

public class UserController {

    @Autowired
    UserService userService;


    @RequestMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(@RequestParam(value = "age",required = false, defaultValue = "0") int age){
        System.out.println("obtener usuarios por edad");

        if (age >= 0){
            List<UserModel> result = new ArrayList<UserModel>();

            for(UserModel userInList : ApitechudbApplication.userModels){
                if (userInList.getAge() == age){
                    result.add(userInList);
                }

            }
            return new ResponseEntity<>(
                    result,
                    HttpStatus.OK
            );
        }
        else{
            return new ResponseEntity<>(
                    this.userService.findAllUser(),
                    HttpStatus.OK
            );
        }
    }


    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id) {
        System.out.println("getUserById");
        System.out.println("La Id del usuario a buscar es " + id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "usuario no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user) {
        System.out.println("addUser");
        System.out.println("La id del usuario a crear es " + user.getId());
        System.out.println("El nombre del usuario a crear es " + user.getName());
        System.out.println("la edad del usuario a crear es " + user.getAge());

        return new ResponseEntity<>(
                this.userService.add(user),
                HttpStatus.CREATED
        );
    }


    @PutMapping("/users/{id}")
        public ResponseEntity<UserModel> updateUser(@RequestBody UserModel user, @PathVariable String id ) {
        System.out.println("La id del usuario que se va a actualizar en parametro es " + id);
        System.out.println("La id del usuario que se va a actualizar es " + user.getId());
        System.out.println("La descripcion del producto que se va a actualizar es " + user.getName());
        System.out.println("El precio del producto que se va a actualizar es " + user.getAge());

        return new ResponseEntity<>(this.userService.update(user), HttpStatus.OK);

    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id) {
        System.out.println("deleteUser");
        System.out.println("la id del usuario a borrar " + id);

        boolean deleteProduct = this.userService.delete(id);

        return new ResponseEntity<>(
                deleteProduct ? "USUARIO BORRADO" : "USUARIO NO ENCONTRADO",
                deleteProduct ?  HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

}
